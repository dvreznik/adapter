package com.elrsoft.adapter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.elrsoft.adapter.db.service.UserService;

public class AddUserActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);

        final EditText name = (EditText) findViewById(R.id.name);
        final EditText phone = (EditText) findViewById(R.id.phone);
        findViewById(R.id.save).
                setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String sName = name.getText().toString();
                                String sPhone = phone.getText().toString();
                                Intent intent = new Intent(AddUserActivity.this, MainActivity.class);
//                              intent.putExtra("user", new User(name.getText().toString(), phone.getText().toString()));
                                new UserService(AddUserActivity.this).save(new User(sName,sPhone));
                                setResult(RESULT_OK,intent);
                                finish();

                            }
                        });
    }
}
