package com.elrsoft.adapter.db.service.core;

import java.util.List;

/**
 * Created by hanz on 20.07.2016.
 */
public interface Service<T> {

    long save(T t);
    List<T> getAll();
}
