package com.elrsoft.adapter.db;

/**
 * Created by hanz on 19.07.2016.
 */
public class Resource {

    public static final String DB_NAME = "db_my_adapter";
    public static final int DB_VER = 1;

    public static final class User {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String PHONE = "phone";
        public static final String TABLE_NAME = "users";

        public static final String CREATE_TABLE = "create table " + TABLE_NAME
                + " ( " + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                NAME + " TEXT(255), " + PHONE + " TEXT(255));";
    }


}
