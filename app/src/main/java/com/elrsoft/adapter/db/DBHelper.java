package com.elrsoft.adapter.db;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.elrsoft.adapter.User;
import com.elrsoft.adapter.db.dao.UserDao;
import com.elrsoft.adapter.db.service.UserService;

/**
 * Created by hanz on 19.07.2016.
 */
public class DBHelper extends SQLiteOpenHelper {


    public DBHelper (Activity activity){
        super(activity, Resource.DB_NAME, null, Resource.DB_VER);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(Resource.User.CREATE_TABLE);

        new UserDao(sqLiteDatabase).save(new User("Yura", "zxcee22e"));
        new UserDao(sqLiteDatabase).save(new User("Yura", "dsf"));
        new UserDao(sqLiteDatabase).save(new User("Yura", "zxcfewee22e"));

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
