package com.elrsoft.adapter.db.service;

import android.app.Activity;

import com.elrsoft.adapter.User;
import com.elrsoft.adapter.db.dao.UserDao;
import com.elrsoft.adapter.db.service.core.OpenDBService;
import com.elrsoft.adapter.db.service.core.Service;

import java.util.List;

/**
 * Created by hanz on 20.07.2016.
 */
public class UserService extends OpenDBService implements Service<User> {

    private Activity activity;

    public UserService(Activity activity) {
        this.activity = activity;
    }

    @Override
    public long save(User user) {
        try {
            if(!isOpen()){
                open(activity);
            }
            return new UserDao(getSqLiteDatabase()).save(user);
        }finally {
            if(isOpen()){
                close();
            }
        }
    }

    @Override
    public List<User> getAll() {
        try {
            if(!isOpen()){
                open(activity);
            }
            return new UserDao(getSqLiteDatabase()).getAll();
        }finally {
            if(isOpen()){
                close();
            }
        }

    }

    public void delete(User user){
        try {
            if(!isOpen()){
                open(activity);
            }
            new UserDao(getSqLiteDatabase())
                    .delete(user);
        }finally {
            if(isOpen()){
                close();
            }
        }
    }
}
