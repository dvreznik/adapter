package com.elrsoft.adapter.db.dao.core;

import android.database.Cursor;

import java.util.List;

/**
 * Created by hanz on 20.07.2016.
 */
public interface Dao<T> {
    long save(T t);
    List<T> getAll();
    List<T> parseCursor(Cursor cursor);
}
