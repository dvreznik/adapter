package com.elrsoft.adapter.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.elrsoft.adapter.User;
import com.elrsoft.adapter.db.Resource;
import com.elrsoft.adapter.db.dao.core.Dao;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hanz on 20.07.2016.
 */
public class UserDao implements Dao<User> {
    SQLiteDatabase sqLiteDatabase;

    public UserDao(SQLiteDatabase sqLiteDatabase) {
        this.sqLiteDatabase = sqLiteDatabase;
    }

    @Override
    public long save(User user) {
        return sqLiteDatabase.insert(
                Resource.User.TABLE_NAME, null,
                setUser(user));
    }

    @Override
    public List<User> getAll() {
        Cursor cursor = sqLiteDatabase.rawQuery(
                "select * from "
                        + Resource.User.TABLE_NAME, null);
        return parseCursor(cursor);
    }

    @Override
    public List<User> parseCursor(Cursor cursor) {
        List<User> users = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(Resource.User.ID));
                String name = cursor.getString(cursor.getColumnIndex(Resource.User.NAME));
                String phone = cursor.getString(cursor.getColumnIndex(Resource.User.PHONE));
                users.add(new User(id, name, phone));

            } while (cursor.moveToNext());
        }
        return users;
    }

    private ContentValues setUser(User user) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(Resource.User.NAME, user.getName());
        contentValues.put(Resource.User.PHONE, user.getPhone());
        return contentValues;

    }

    public void delete(User user) {
        sqLiteDatabase.delete(
                Resource.User.TABLE_NAME,
                Resource.User.ID + " = ?",
                new String[]{String.
                        valueOf(user.getId())}
        );
    }
}
