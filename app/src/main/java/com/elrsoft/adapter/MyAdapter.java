package com.elrsoft.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.elrsoft.adapter.db.service.UserService;

import java.util.List;

/**
 * Created by hanz on 18.07.2016.
 */
public class MyAdapter extends BaseAdapter {
    private List<User> users;
    private LayoutInflater layoutInflater;
    private Activity activity;

    public MyAdapter(List<User> users, Activity activity) {
        this.users = users;
        this.activity = activity;
        layoutInflater = (LayoutInflater) activity.
                getSystemService(Context.
                        LAYOUT_INFLATER_SERVICE);
    }

    public void deleteUser(){
        users = new UserService(activity).getAll();
        notifyDataSetChanged();
    }

    public void addUser(User user){
        users.add(user);
        notifyDataSetChanged();
    }

    private class ViewHolder{
        TextView name;
        TextView phone;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
      public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position,
                        View convertView,
                        ViewGroup parent) {

        ViewHolder viewHolder = null;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.item, null, false);

            viewHolder.name = (TextView)
                    convertView.findViewById(R.id.name);
            viewHolder.phone = (TextView)
                    convertView.findViewById(R.id.phone);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        User user = (User) getItem(position);

        viewHolder.name.setText(user.getName());
        viewHolder.phone.setText(user.getPhone());

        return convertView;
    }
}
