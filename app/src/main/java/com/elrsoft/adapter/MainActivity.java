package com.elrsoft.adapter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.elrsoft.adapter.db.service.UserService;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    MyAdapter myAdapter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_settings:
                startActivity(new Intent(MainActivity.this,
                        AddUserActivity.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Menu");
        menu.add(1, 1, 0, "Delete");

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case 1:
                AdapterView.AdapterContextMenuInfo info
                        = (AdapterView.AdapterContextMenuInfo)
                        item.getMenuInfo();
                //myAdapter.deleteUser(info.position);
                new UserService(MainActivity.this).delete(
                        (User) myAdapter.getItem(
                                info.position
                        )
                );
                myAdapter.deleteUser();
                return true;
            default:
                return false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = (ListView)
                findViewById(R.id.listView);
        myAdapter = new MyAdapter(new UserService(this).getAll()
                , this);
        listView.setAdapter(myAdapter);
        User user = getIntent().getParcelableExtra("user");
        if(user != null){
            myAdapter.addUser(user);
        }

        registerForContextMenu(listView);
        listView.setOnItemClickListener(new AdapterView.
                OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view, int position, long id) {
//                Toast.makeText(MainActivity.this,
//                        ((User) parent.getItemAtPosition(position)).getPhone(),
//                        Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this
                , Main2Activity.class);
                intent.putExtra("user", ((User) parent.getItemAtPosition(position)));
                startActivity(intent);
                finish();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            myAdapter.deleteUser();
        }
    }
}
